# Tools container

The `tools-container` is an essential part of Grove, used by the CI/CD pipelines, providing a wrapper for the tooling. Furthermore, Grove's CLI command is installed within the Docker container defined by `tools-container`. This setup ensures we are not polluting the user's environment with commands it may not need.

## Integration with CI/CD pipelines

The CI/CD pipelines are tied to a specific `tools-container` version. Although the default version is set in the [`tools-container/ci_vars.yml`](https://gitlab.com/opencraft/dev/grove/-/blob/main/tools-container/ci_vars.yml) file of Grove, it can be pinned by changing either of the `TOOLS_CONTAINER_IMAGE_NAME` or `TOOLS_CONTAINER_IMAGE_VERSION` variables in the CI/CD settings of the given GitLab project.

## Creating branch specific version

When the CI/CD test pipeline is running for Grove, the first step is building and publishing the `tools-container` image. In most cases, it is not needed to build the image, hence it must be triggered manually.

Therefore, in case you need to test a pipeline with changes on a given Grove branch, simply click the "play icon" for building the image and triggering that pipeline step.

## Temporary version override

The `tools-container` version is integrated with the CI/CD pipeline, though this causes a deadlock when a feature development depends on a new container version. If the version is set in the general CI/CD variables, it would affect the whole repository. To resolve this deadlock, there is a possibility to temporarily override the setting for a given pipeline run, leaving the other pipelines intact.

To set a temporary override, follow the instructions below:

1. navigate to the Grove cluster repository
1. on the left hand side, click the "CI/CD" menu item, then "Pipelines"
1. click on "Run pipeline" close to the upper right corner
1. select the desired branch to run the pipeline on
1. after selecting the branch, set the desired environment variable name and value
1. lastly, click on "Run pipeline"

The new pipeline run will have the overridden `tools-container` version and will download that from the docker registry.
