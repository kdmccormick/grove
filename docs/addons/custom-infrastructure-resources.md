# Custom infrastructure resources

Although Grove manages all the necessary resources belonging to an instance, there are some cases that requires custom resources. For example, when setting up some edX plugins, you may want to have a separate public bucket to be used by the plugin, rather than making the Grove-created bucket public.

Because of these cases, Grove implements a simple way to let a Grove cluster repository define custom resources.

## The `infrastructure` directory

The cluster repository may define an `infrastructure` directory. This directory has a special meaning: it is imported by Grove to parse all Terraform files defined in it.

It is like copy-pasting custom Terraform resources into Grove, without git-committing it. Actually, that's what Grove is doing behind the curtains.

The infrastructure directory will be used as a Terraform module by Grove. To make it easier to integrate custom resources with Grove, Grove passes variables to the custom module. To learn more about what variables are passed, take a look at [plugin's README](https://gitlab.com/opencraft/dev/grove/-/blob/main/provider-plugin/README.md).

!!! warning
    This integration is meant for instance-specific infrastructure. If a feature is meant to be used by multiple instances, that feature should be implemented directly in Grove.
