# Cron jobs

Grove allows you to define cron jobs to run in par with your instance by
defining an array of objects containing the job schedule and script to execute.

## Use cases

There are many reasons you may want this behaviour, for example running
`manage.py` commands periodically, like aggregator cleanups. Take the following
config example:

.. code-block:: yaml

    # instance/config.yml
    GROVE_CRON_JOBS:
        - name: run_aggregator_service
          schedule: '*/10 * * * *'
          script: ./manage.py lms run_aggregator_service
        - name: hello_world
          schedule: '*/20 * * * *'
          script: echo Hello world

## Notes

The cron jobs are running in separate pods. Therefore it is not sharing volumes
with LMS/Studio or other pods. If you want to run jobs that must access data
stored in LMS/Studio volumes, these cron jobs won't be sufficient for your
needs. Grove is not prepared for this scenario (yet).

Right now, only the edx-platform image is used for the cron jobs, meaning that
running scripts for other services (like Course Discovery or Enterprise
Catalog) is not supported yet.
