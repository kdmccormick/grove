"""
Methods for working with the k8s metrics API>
"""
import datetime

from kubernetes.client import ApiClient, Configuration, CustomObjectsApi

from .namespace_metric import NamespaceMetric
from .utils import get_secret, get_secret_path

NAME = "k8s-metrics"


def configure_client() -> CustomObjectsApi:
    """
    Configure the Kubernetes client and return a core v1 API client for later use.

    Although returning the client wouldn't be necessary, since only v1 resources are
    used in Grove it is safe to return the v1 API client.

    Returns:
        Core v1 API client for later use.
    """

    # OpenFAAS expands the service account secret, therefore the data.token key is
    # available as token. The value is coming from periodic-build-notifier-access-token.
    conf = Configuration(
        host="https://kubernetes.default.svc",
        api_key={"authorization": get_secret("token")},
        api_key_prefix={"authorization": "Bearer"},
    )

    conf.ssl_ca_cert = get_secret_path("ca.crt")

    return CustomObjectsApi(api_client=ApiClient(conf))


def fetch_namespace_metrics(timestamp: datetime.datetime) -> list[NamespaceMetric]:
    """
    Use the k8s api to fetch the metrics for the pods.
    """
    k8s_api = configure_client()
    pod_metrics = k8s_api.list_cluster_custom_object(
        "metrics.k8s.io", "v1beta1", "pods"
    )
    report_items = {}
    for api_item in pod_metrics["items"]:
        namespace_metric = NamespaceMetric.import_metric(api_item)
        if namespace_metric.namespace in report_items:
            report_items[namespace_metric.namespace].combine(namespace_metric)
        else:
            report_items[namespace_metric.namespace] = namespace_metric
    return list(report_items.values())
