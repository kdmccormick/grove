"""
Helpers for working with the AWS S3 API.
"""
import os
import typing

import boto3

from .utils import get_secret


class S3:
    """
    Helper class to add convenience methods for S3.
    """

    def __init__(self):
        self.s3_client = None
        self.s3_resource = None
        self.s3_bucket_name = None

    def _get_s3_config(self) -> boto3.resources.base.ServiceResource:
        """
        Retrieve the S3 config variables from the environment.
        """
        access_key_id = os.getenv("AWS_ACCESS_KEY_ID", "") or get_secret(
            "usage-report-aws-access-key-id"
        )
        secret_access_key = os.getenv("AWS_SECRET_KEY", "") or get_secret(
            "usage-report-aws-secret-key"
        )
        region = os.getenv("AWS_REGION", "")
        endpoint_url = os.getenv("AWS_ENDPOINT_URL", "")

        s3_config = {
            "region_name": region,
            "aws_access_key_id": access_key_id,
            "aws_secret_access_key": secret_access_key,
        }

        if endpoint_url:
            s3_config["endpoint_url"] = endpoint_url
        return s3_config

    def init_s3_api(self):
        """
        Initialises the S3 API, by setting the s3_client and s3_resource
        vars on the class. Needs to only run once.
        """
        s3_config = self._get_s3_config()
        self.s3_client = boto3.client("s3", **s3_config)
        self.s3_resource = boto3.resource("s3", **s3_config)
        self.s3_bucket_name = os.getenv("S3_BUCKET_NAME")

    def list_directory(
        self, directory: str
    ) -> boto3.resources.collection.ResourceCollection:
        """
        List the objects within an S3 directory and returns
        a list of S3 objects.
        """
        if self.s3_client is None:
            self.init_s3_api()

        s3_bucket = self.s3_resource.Bucket(self.s3_bucket_name)
        max_page_size = 1000
        return s3_bucket.objects.page_size(max_page_size).filter(Prefix=directory)

    def upload_to_bucket(self, filename: str, file_object: typing.IO) -> str:
        """
        Uploads file_object to the S3 bucket, bucket_name, using
        the filename provided. Returns a presigned URL to the
        created S3 object.
        """
        if self.s3_client is None:
            self.init_s3_api()

        file_object.seek(0)

        s3_object = self.s3_resource.Bucket(self.s3_bucket_name).put_object(
            Key=filename, Body=file_object.read().encode(file_object.encoding)
        )

        return self.s3_client.generate_presigned_url(
            "get_object",
            Params={"Bucket": self.s3_bucket_name, "Key": s3_object.key},
            ExpiresIn=60 * 60,
        )
