"""
Functions to export usage metrics.
"""
import csv
import typing

from .namespace_metric import NamespaceMetric


def export_to_csv(csv_file: typing.IO, metrics: list[NamespaceMetric]) -> typing.IO:
    """
    Exports the usage information in `report_items` the `csv_file`
    provided.
    """
    writer = csv.writer(csv_file)
    writer.writerow(["Namespace", "CPU", "Memory", "Equivalent VMs"])
    for metric in metrics:
        writer.writerow(
            [
                metric.namespace,
                metric.cpu_minutes,
                metric.memory_megabytes,
                metric.num_equivalent_vms(),
            ]
        )
    return csv_file
