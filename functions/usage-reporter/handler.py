"""
OpenFaas hooks.
"""
import datetime
import json
import os
from tempfile import NamedTemporaryFile

from . import faas_request, monthly_stats, utils
from .exporter import export_to_csv
from .s3 import S3


def handle(req: str) -> str:
    """
    Handler function for OpenFaas. Generated the usage report and
    uploads to S3.

    Return a JSON string with attributes:
      message: str -> "success" if report was generated successfully.
      url: str -> The S3 URL to the generated report.
    """

    data = json.loads(req) if req else {}

    try:
        provider = faas_request.get_provider(data)
        timestamp = faas_request.get_timestamp(data)
        num_months = faas_request.get_num_months(data)
    except faas_request.RequestValueError as request_error:
        return json.dumps({"message": str(request_error)})

    report_items = (
        monthly_stats.fetch_average_metrics(provider.NAME, num_months, timestamp)
        if num_months
        else provider.fetch_namespace_metrics(timestamp)
    )

    filename = utils.get_report_filename(
        provider.NAME, datetime.datetime.now(), bool(num_months)
    )

    s3_client = S3()
    with NamedTemporaryFile(mode="w+", encoding="utf8") as csv_file:
        export_to_csv(csv_file, report_items)
        object_url = s3_client.upload_to_bucket(filename, csv_file)

    return json.dumps(
        {
            "message": "success",
            "report_url": object_url,
            "items": [z.asdict() for z in report_items],
        }
    )
