"""
Methods for working with the Prometheus API
"""
import datetime
import os
import time
import urllib

import requests

from .namespace_metric import NamespaceMetric

PROMETHEUS_QUERY_URL = os.getenv(
    "PROMETHEUS_HOST", "http://prometheus-operated.monitoring.svc:9090/api/v1/query?"
)
NAME = "prometheus"


def prometheus_api_request(promql: str, timestamp: datetime.datetime) -> dict:
    """
    Runs the `promql` query on the promethues API at the specified time
    and returns the results in a dictionary of the format namespace: value.
    """
    promql = urllib.parse.quote(promql)
    unix_timestamp = time.mktime(timestamp.timetuple())
    url = f"{PROMETHEUS_QUERY_URL}time={unix_timestamp}&query={promql}&step=5m"
    response = requests.get(url, timeout=60)
    results = {}
    for result in response.json()["data"]["result"]:
        if "namespace" not in result["metric"]:
            continue
        namespace = result["metric"]["namespace"]
        results[namespace] = float(result["value"][1])
    return results


def fetch_cpu_metrics(timestamp: datetime.datetime) -> dict:
    """
    Fetch the memory metrics using Prometheus API.
    Returns a dictionary keyed by the namespace and cpu_minutes as the value.
    """
    promql = (
        'sum(rate(container_cpu_usage_seconds_total{image!=""}[5m])) by (namespace)'
    )
    return prometheus_api_request(promql, timestamp)


def fetch_memory_metrics(timestamp: datetime.datetime) -> dict:
    """
    Fetch the memory metrics using Prometheus API.
    """
    promql = 'sum(container_memory_working_set_bytes{image!=""}) by (namespace)'
    return prometheus_api_request(promql, timestamp)


def fetch_namespace_metrics(timestamp: datetime.datetime) -> list[NamespaceMetric]:
    """
    Fetch the metrics using Prometheus API and return
    them in a usable format.
    """
    cpu_metrics = fetch_cpu_metrics(timestamp)
    memory_metrics = fetch_memory_metrics(timestamp)

    metrics = []
    for namespace, cpu_minutes in cpu_metrics.items():
        metrics.append(
            NamespaceMetric(
                cpu_minutes=cpu_minutes * 1000,
                memory_megabytes=memory_metrics.get(namespace, 0) / 1024 / 1024,
                namespace=namespace,
            )
        )
    return metrics
