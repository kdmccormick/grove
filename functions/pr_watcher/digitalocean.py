"""
DigitalOcean module provides a client for interacting with the DigitalOcean API.
"""

import boto3
import botocore


class DigitalOceanClient:
    """
    Client for interacting with the DigitalOcean API.
    """

    def __init__(
        self,
        spaces_endpoint: str,
        spaces_region: str,
        access_key_id: str,
        secret_access_key: str,
    ) -> None:
        self.boto3_client: boto3.client = boto3.session.Session().client(
            "s3",
            endpoint_url=spaces_endpoint,
            config=botocore.config.Config(s3={"addressing_style": "virtual"}),
            region_name=spaces_region,
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key,
        )

    def upload_file_to_spaces(
        self, content: str, filename: str, spaces_endpoint: str, bucket: str, acl: str
    ) -> str:
        """
        Upload given content to spaces bucket with the given filename and
        returns the URL of the uploaded file.
        """
        self.boto3_client.put_object(
            Bucket=bucket, Key=filename, Body=bytes(content, "utf-8"), ACL=acl
        )
        bucket_url = spaces_endpoint.replace("https://", f"https://{bucket}.")
        return f"{bucket_url}/{filename}"
