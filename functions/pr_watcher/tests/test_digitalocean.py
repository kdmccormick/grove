from unittest.mock import MagicMock, patch

from ..conf import config
from ..digitalocean import *


@patch("pr_watcher.digitalocean.boto3.session.Session")
def test_upload_file_to_spaces(mock_session):
    mock_client = MagicMock()
    mock_client.put_object.return_value = None
    mock_session.return_value.client.return_value = mock_client

    client = DigitalOceanClient(
        spaces_endpoint=config.digitalocean.get_spaces_endpoint(),
        spaces_region=config.digitalocean.spaces_endpoint_region,
        access_key_id="test",
        secret_access_key="test",
    )

    url = client.upload_file_to_spaces(
        "testcontent",
        "testfilename",
        config.digitalocean.get_spaces_endpoint(),
        config.log_bucket_name,
        config.digitalocean.spaces_upload_default_acl,
    )

    assert (
        url == "https://grove-stage-build-logs.nyc3.digitaloceanspaces.com/testfilename"
    )
    mock_client.put_object.assert_called_once_with(
        Bucket="grove-stage-build-logs",
        Key="testfilename",
        Body=b"testcontent",
        ACL="public-read",
    )
