from base64 import b64encode
from unittest.mock import Mock, call, patch

from ..conf import config
from ..github import PullRequest
from ..gitlab import (
    CreateOrUpdatePipelineParams,
    DestroyPipelineParams,
    GitLabClient,
    GitLabFile,
    GitLabStatus,
    GitLabWebhook,
    GroveInstance,
    InstanceConfigFile,
)


def test_gitlab_file_content_as_dict():
    file = GitLabFile(
        {
            "file_name": "test.txt",
            "file_path": "/testdir/test.txt",
            "size": 10,
            "encoding": "utf-8",
            "content": b64encode(b"test: content"),
            "content_sha256": "test-sha256",
            "ref": "test-ref",
            "blob_id": "test-blob-id",
            "commit_id": "test-commit-id",
            "last_commit_id": "test-last-commit-id",
        }
    )

    assert file.content_as_dict() == {"test": "content"}


def test_gitlab_file_content_as_string():
    file = GitLabFile(
        {
            "file_name": "test.txt",
            "file_path": "/testdir/test.txt",
            "size": 10,
            "encoding": "utf-8",
            "content": b64encode(b"test-content"),
            "content_sha256": "test-sha256",
            "ref": "test-ref",
            "blob_id": "test-blob-id",
            "commit_id": "test-commit-id",
            "last_commit_id": "test-last-commit-id",
        }
    )

    assert file.content_as_string() == "test-content"


def test_grove_instance_is_defined():
    mock_client = Mock()
    instance = GroveInstance(mock_client, "test-instance")

    assert instance.is_defined is True
    assert mock_client.get_instance_config.call_args_list == [
        call(config.grove.project_id, "test-instance", InstanceConfigFile.TUTOR),
        call(config.grove.project_id, "test-instance", InstanceConfigFile.GROVE),
        call(config.grove.project_id, "test-instance", InstanceConfigFile.REQUIREMENTS),
    ]


def test_gitlab_webhook_comments_url(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert (
        webhook.comments_url
        == "https://api.github.com/repos/open-craft/edx-platform/issues/608/comments"
    )


def test_gitlab_webhook_instance_name(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert webhook.instance_name == "pr-grove-test-upgrade"


def test_gitlab_webhook_is_pr_instance(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert webhook.is_pr_instance is True


def test_gitlab_webhook_is_deployment_failed(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert webhook.is_deployment_failed is False


def test_gitlab_webhook_is_deployment_succeeded(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert webhook.is_deployment_succeeded is True


def test_gitlab_webhook_is_valid(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    assert webhook.is_valid is True


@patch("pr_watcher.gitlab.requests")
def test_gitlab_client_get_object(mock_requests):
    expected_url = f"https://{config.gitlab.host}/api/v4/path"
    expected_response = {
        "test": "test-content",
    }

    mock_response = Mock()
    mock_response.json.return_value = expected_response
    mock_response.headers = {"content-type": "application/json"}
    mock_response.status_code = 200

    mock_requests.get.return_value = mock_response

    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    obj = client.get_object("/path")

    assert obj == expected_response
    mock_requests.get.assert_called_once_with(expected_url, **client.api_params)


@patch("pr_watcher.gitlab.requests")
def test_gitlab_client_get_instance_config(
    mock_requests, gitlab_grove_config_file_info
):
    expected_url = "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Finstance%2Fgrove.yml?ref=main"

    mock_response = Mock()
    mock_response.json.return_value = gitlab_grove_config_file_info
    mock_response.headers = {"content-type": "application/json"}
    mock_response.status_code = 200

    mock_requests.get.return_value = mock_response

    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    config_file = client.get_instance_config(
        config.grove.project_id,
        "instance",
        InstanceConfigFile.GROVE,
    )

    assert config_file.__dict__ == GitLabFile(gitlab_grove_config_file_info).__dict__
    mock_requests.get.assert_called_once_with(expected_url, **client.api_params)


def test_gitlab_client_get_pipeline_job_id(
    gitlab_webhook_payload,
    gitlab_tutor_config_file,
    gitlab_grove_config_file,
    gitlab_requirements_file,
):
    mock_client = Mock()
    mock_client.get_instance_config.side_effect = [
        gitlab_tutor_config_file,
        gitlab_grove_config_file,
        gitlab_requirements_file,
    ]

    webhook = GitLabWebhook(mock_client, gitlab_webhook_payload)
    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    job_id = client.get_pipeline_job_id(webhook.jobs, GitLabStatus.SUCCESS)
    assert job_id == 5723021211


@patch("pr_watcher.gitlab.requests")
def test_gitlab_client_get_pipeline_job_logs(mock_requests):
    expected_url = "https://gitlab.com/api/v4/projects/test/jobs/5723021211/trace"
    expected_response = "log file content"

    mock_response = Mock()
    mock_response.text = expected_response
    mock_response.headers = {"content-type": "text/plain"}
    mock_response.status_code = 200

    mock_requests.get.return_value = mock_response

    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    logs = client.get_pipeline_job_logs(5723021211)
    assert logs == expected_response
    mock_requests.get.assert_called_once_with(expected_url, **client.api_params)


@patch("pr_watcher.gitlab.datetime")
@patch("pr_watcher.gitlab.requests")
def test_gitlab_client_trigger_instance_create_or_update_pipeline(
    mock_requests,
    mock_datetime,
    gitlab_grove_config_file_info,
    open_platform_pull_request_info,
):
    mock_datetime.now.return_value.strftime.return_value = "20231213-1811"

    mock_response = Mock()
    mock_response.json.return_value = gitlab_grove_config_file_info
    mock_response.headers = {"content-type": "application/json"}
    mock_response.status_code = 200

    mock_requests.get.return_value = mock_response

    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    instance = GroveInstance(client, "test-instance")
    params = CreateOrUpdatePipelineParams(
        instance=instance,
        pull_request=PullRequest(open_platform_pull_request_info),
        edx_platform_url="https://example.com",
        edx_platform_branch="master",
    )

    client.trigger_instance_create_or_update_pipeline(params)

    assert mock_requests.get.call_args_list == [
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Fconfig.yml?ref=main",
            **client.api_params,
        ),
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Fgrove.yml?ref=main",
            **client.api_params,
        ),
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Frequirements.txt?ref=main",
            **client.api_params,
        ),
    ]

    mock_requests.post.assert_called_once_with(
        "https://gitlab.com/api/v4/projects/test/trigger/pipeline",
        json={
            "GROVE_PR_WATCHER": {
                "applied_settings": "EDXAPP_EXTRA_SETTINGS: my extra settings",
                "applied_requirements": "tutor>=16,<17\ntutor-discovery>=16,<17\ntutor-ecommerce>=16,<17\ntutor-mfe>=16,<17\ntutor-xqueue>=16,<17\ntutor-forum>=16,<17\ngit+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v16.0.0\ngit+https://github.com/hastexo/tutor-contrib-s3.git@v1.2.0",
                "comments_url": "https://api.github.com/repos/open-craft/edx-platform/issues/608/comments",
                "commit_ref": "0ce6bf5f13ac5917dd09a313618f607b891cd2a4",
                "url": "https://github.com/open-craft/edx-platform/pull/608",
            },
            "TUTOR_EDX_PLATFORM_REPOSITORY": "https://example.com",
            "TUTOR_EDX_PLATFORM_VERSION": "master",
            "TUTOR_ELASTICSEARCH_INDEX_PREFIX": "test-instance",
            "TUTOR_OPENEDX_COMMON_VERSION": "open-release/palm.4",
            "variables": {
                "INSTANCE_NAME": "test-instance",
                "DEPLOYMENT_REQUEST_ID": "20231213-1811",
                "NEW_INSTANCE_TRIGGER": "0",
                "REQUIREMENTS": "tutor>=16,<17\ntutor-discovery>=16,<17\ntutor-ecommerce>=16,<17\ntutor-mfe>=16,<17\ntutor-xqueue>=16,<17\ntutor-forum>=16,<17\ngit+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v16.0.0\ngit+https://github.com/hastexo/tutor-contrib-s3.git@v1.2.0",
            },
            "TUTOR_EDXAPP_EXTRA_SETTINGS": "my extra settings",
            "ref": "main",
            "token": "trigger-token",
        },
        **client.api_params,
    )


@patch("pr_watcher.gitlab.datetime")
@patch("pr_watcher.gitlab.requests")
def test_gitlab_client_trigger_instance_destroy_pipeline(
    mock_requests,
    mock_datetime,
    gitlab_grove_config_file_info,
    merged_platform_pull_request_info,
):
    mock_datetime.now.return_value.strftime.return_value = "20231213-1811"

    mock_response = Mock()
    mock_response.json.return_value = gitlab_grove_config_file_info
    mock_response.headers = {"content-type": "application/json"}
    mock_response.status_code = 200

    mock_requests.get.return_value = mock_response

    client = GitLabClient(
        host=config.gitlab.host,
        access_token="access-token",
        trigger_token="trigger-token",
        user_agent=config.user_agent,
    )

    instance = GroveInstance(client, "test-instance")
    params = DestroyPipelineParams(instance=instance)

    client.trigger_instance_destroy_pipeline(params)

    assert mock_requests.get.call_args_list == [
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Fconfig.yml?ref=main",
            **client.api_params,
        ),
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Fgrove.yml?ref=main",
            **client.api_params,
        ),
        call(
            "https://gitlab.com/api/v4/projects/test/repository/files/instances%2Ftest-instance%2Frequirements.txt?ref=main",
            **client.api_params,
        ),
    ]

    mock_requests.post.assert_called_once_with(
        "https://gitlab.com/api/v4/projects/test/trigger/pipeline",
        json={
            "variables": {
                "INSTANCE_NAME": "test-instance",
                "DEPLOYMENT_REQUEST_ID": mock_datetime.now.return_value.strftime.return_value,
                "DELETE_INSTANCE_TRIGGER": "1",
            },
            "ref": "main",
            "token": "trigger-token",
        },
        **client.api_params,
    )
