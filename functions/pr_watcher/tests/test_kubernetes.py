from datetime import datetime, timedelta, timezone
from unittest.mock import MagicMock, patch

from ..kubernetes import KubernetesClient


@patch("pr_watcher.kubernetes.get_secret")
@patch("pr_watcher.kubernetes.get_secret_path")
@patch("pr_watcher.kubernetes.CoreV1Api.list_namespace")
def test_get_namespaces(mock_list_namespace, mock_get_secret_path, mock_get_secret):
    mock_get_secret_path.return_value = "/var/openfaas/secrets/ca.crt"
    mock_get_secret.return_value = "testtoken"

    mock_namespace1 = MagicMock()
    mock_namespace1.metadata.name = "testnamespace1"

    mock_namespace2 = MagicMock()
    mock_namespace2.metadata.name = "testnamespace2"

    mock_list_namespace.return_value.items = [
        mock_namespace1,
        mock_namespace2,
    ]

    client = KubernetesClient()
    namespaces = client.get_namespaces()
    assert namespaces == ["testnamespace1", "testnamespace2"]


@patch("pr_watcher.kubernetes.get_secret")
@patch("pr_watcher.kubernetes.get_secret_path")
@patch("pr_watcher.kubernetes.CoreV1Api.list_namespaced_pod")
def test_get_pod_in_namespace(
    mock_list_namespaced_pod, mock_get_secret_path, mock_get_secret
):
    mock_get_secret_path.return_value = "/var/openfaas/secrets/ca.crt"
    mock_get_secret.return_value = "testtoken"

    mock_pod = MagicMock()
    mock_pod.metadata.name = "testpod"

    mock_list_namespaced_pod.return_value.items = [mock_pod]

    client = KubernetesClient()
    pod = client.get_pod_in_namespace("testselector", "testnamespace")
    assert pod.metadata.name == "testpod"
