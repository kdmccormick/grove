"""
Configuration of the function paresed from the environment variables.

The environment variables that are prefixed with `PR_WATCHER_` are parsed as
settings for the root configuration object. All configuration objects have their
corresponding environment variables prefixed with their name in uppercase.

For example, the configuration object `openedx` has its environment variables
prefixed with `OPENEDX_`. The configuration object `openedx_instance` has its
environment variables prefixed with `OPENEDX_INSTANCE_`.

The module have a `config` object that contains the parsed configuration. When
the module is imported, the configuration is parsed from the environment
variables and stored in the `config` object. Hence, before importing this
module, the environment variables must be set.
"""

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class OpenEdxConfig(BaseSettings):
    """
    OpenEdX specific configuration.
    """

    watched_forks: list[str] = Field(
        ["openedx/edx-platform"],
        description="List of forks of edx-platform to watch for PRs.",
    )
    mfe_repo_name_prefix: str = Field(
        "frontend-app-",
        description="Prefix of the MFE repositories.",
    )
    default_platform_url: str = Field(
        "https://github.com/openedx/edx-platform.git",
        description="Default URL of the edx-platform repository.",
    )
    default_platform_branch: str = Field(
        "master",
        description="Default branch of the edx-platform repository.",
    )

    model_config = SettingsConfigDict(env_prefix="OPENEDX_")


class GroveConfig(BaseSettings):
    """
    Grove specific configuration.
    """

    watched_pr_key: str = "PR_WATCHER"

    project_id: str = Field(
        ...,
        description="ID of the project that contains the template files.",
    )
    main_branch: str = Field(
        "main",
        description="Name of the main branch of the Grove project.",
    )

    model_config = SettingsConfigDict(env_prefix="GROVE_")


class GitHubConfig(BaseSettings):
    """
    GitHub specific configuration.
    """

    authorized_users: list[str] = Field(
        [],
        description="List of usernames that are authorized to have sandboxes.",
    )
    authorized_teams: list[str] = Field(
        [],
        description="List of teams that are authorized to have sandboxes.",
    )
    watched_organizations: list[str] = Field(
        [],
        description="List of organizations that are watched for PRs.",
    )

    model_config = SettingsConfigDict(env_prefix="GITHUB_")


class GitLabConfig(BaseSettings):
    """
    GitLab specific configuration.
    """

    host: str = Field(
        "gitlab.com",
        description="Host of the GitLab instance.",
    )

    model_config = SettingsConfigDict(env_prefix="GITLAB_")


class DigitalOceanConfig(BaseSettings):
    """
    Digital Ocean specific configuration.
    """

    spaces_endpoint_pattern: str = Field(
        "https://{region}.digitaloceanspaces.com",
        description="Endpoint of the Digital Ocean Spaces.",
    )
    spaces_endpoint_region: str = Field(
        ...,
        description="Region of the Digital Ocean Spaces.",
    )
    spaces_upload_default_acl: str = Field(
        "public-read",
        description="Default ACL to use when uploading files to Digital Ocean Spaces.",
    )

    def get_spaces_endpoint(self) -> str:
        """
        Returns the endpoint of the Digital Ocean Spaces.
        """

        return self.spaces_endpoint_pattern.format(
            region=self.spaces_endpoint_region,
        )

    model_config = SettingsConfigDict(env_prefix="DIGITALOCEAN_")


class Config(BaseSettings):
    """
    Configuration of the function paresed from the environment variables.

    Those environment variables that are prefixed with `PR_WATCHER_` are parsed
    as settings.
    """

    user_agent: str = "grove-pr-watcher/v1.0.0"
    log_bucket_name: str = Field(
        "grove-stage-build-logs",
        description="Name of the bucket to store the logs.",
    )
    sandbox_watched_topic: str = Field(
        "pr-sandboxes-enabled",
        description="Topic to watch for sandboxes.",
    )
    sandbox_create_topic: str = Field(
        "create-sandbox",
        description="Topic to create sandboxes.",
    )
    sandbox_destroy_cutoff_days: int = Field(
        14,
        description="Number of days after which the sandbox is destroyed.",
    )
    log_level: str = Field(
        "INFO",
        description="Level of the logs.",
    )

    # OpenEdX configuration
    openedx: OpenEdxConfig = OpenEdxConfig()

    # Grove configuration
    grove: GroveConfig = GroveConfig()

    # Provider specific configuration
    github: GitHubConfig = GitHubConfig()
    gitlab: GitLabConfig = GitLabConfig()
    digitalocean: DigitalOceanConfig = DigitalOceanConfig()

    # Settings configuration
    model_config = SettingsConfigDict(env_prefix="PR_WATCHER_")


config = Config()
