"""
GitLab module provides a client for interacting with the GitLab API.

The GitLab API is used to retrieve information about merge requests, get config
for Grove cluster repositories, instances, and listen for merge request events.
"""
import json
import logging
import re
from base64 import b64decode
from datetime import datetime
from enum import StrEnum
from typing import Any
from urllib.parse import quote_plus

import requests
import yaml
from methodtools import lru_cache
from pydantic import BaseModel, ConfigDict, Field

from .conf import config
from .exceptions import ObjectDoesNotExist, RateLimitExceeded
from .github import PullRequest
from .openedx import MFEInfo
from .utils import get_nonce, get_ttl_hash

logger = logging.getLogger(__name__)

SANDBOX_INSTANCE_PATTERN: str = r"(?P<instance>pr-[,\w\d-]+)"


class GitLabStatus(StrEnum):
    """
    Possible statuses of a GitLab CI/CD pipeline or job.
    """

    PENDING = "pending"
    RUNNING = "running"
    SUCCESS = "success"
    FAILED = "failed"

    def __str__(self) -> str:
        return self.value


class InstanceConfigFile(StrEnum):
    """
    Possible config files in a Grove cluster repository for an instance.
    """

    TUTOR = "config.yml"
    GROVE = "grove.yml"
    REQUIREMENTS = "requirements.txt"

    def __str__(self) -> str:
        return self.value

    def get_path(self, instance_name: str) -> str:
        """
        Returns the path to the config file for the instance.
        """
        return quote_plus(f"instances/{instance_name}/{self.value}")


class GroveInstance:
    """
    The instance that is running on a Grove cluster.
    """

    def __init__(self, client: "GitLabClient", instance_name: str) -> None:
        self.__client: GitLabClient = client
        self.name: str = instance_name

        self.tutor_config: GitLabFile | None = self.__client.get_instance_config(
            config.grove.project_id,
            self.name,
            InstanceConfigFile.TUTOR,
        )
        self.grove_config: GitLabFile | None = self.__client.get_instance_config(
            config.grove.project_id,
            self.name,
            InstanceConfigFile.GROVE,
        )
        self.requirements: GitLabFile | None = self.__client.get_instance_config(
            config.grove.project_id,
            self.name,
            InstanceConfigFile.REQUIREMENTS,
        )

    @property
    def is_defined(self) -> bool:
        """
        Returns True if the instance is defined, otherwise False.
        """
        return (
            self.tutor_config is not None
            and self.grove_config is not None
            and self.requirements is not None
        )


class CreateOrUpdatePipelineParams(BaseModel):
    """
    Parameters for triggering the create or update pipeline.
    """

    instance: GroveInstance = Field(...)
    pull_request: PullRequest = Field(...)
    edx_platform_url: str = Field(...)
    edx_platform_branch: str = Field(...)
    mfe_info: MFEInfo | None = Field(None)

    model_config = ConfigDict(arbitrary_types_allowed=True)


class DestroyPipelineParams(BaseModel):
    """
    Parameters for triggering the destroy pipeline.
    """

    instance: GroveInstance = Field(...)

    model_config = ConfigDict(arbitrary_types_allowed=True)


class GitLabFile:
    """
    Utility class representing a GitLab file.

    See https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
    """

    def __init__(self, info: dict):
        self.name: str = info["file_name"]
        self.path: str = info["file_path"]
        self.size: int = info["size"]
        self.encoding: str = info["encoding"]
        self.content: bytes = info["content"]
        self.content_sha256: str = info["content_sha256"]
        self.ref: str = info["ref"]
        self.blob_id: str = info["blob_id"]
        self.commit_id: str = info["commit_id"]
        self.last_commit_id: str = info["last_commit_id"]

    def content_as_dict(self) -> dict:
        """
        Returns the fetched files content as a dictionary.
        """
        return yaml.safe_load(self.content_as_string())

    def content_as_string(self) -> str:
        """
        Returns the fetched files content as a string.
        """
        return b64decode(self.content).decode("utf8")


class GitLabClient:
    """
    Client for interacting with the GitLab API.
    """

    def __init__(
        self, host: str, access_token: str, trigger_token: str, user_agent: str
    ) -> None:
        self.trigger_token = trigger_token
        self.api_base_url = f"https://{host}/api/v4"
        self.api_params = {
            "headers": {
                "PRIVATE-TOKEN": access_token,
                "User-Agent": user_agent,
                "Time-Zone": "UTC",
            },
            "timeout": 30,
        }

    @lru_cache()
    def get_object(self, url: str, ttl=None) -> dict:
        """
        Send the request to the provided URL, attaching custom headers, and
        returns the deserialized object from the returned JSON.

        Raises ObjectDoesNotExist if github returns a 404 response.
        """
        del ttl  # TTL is used to invalidate the cache.

        resp = requests.get(self.api_base_url + url, **self.api_params)

        if resp.status_code == 404:
            raise ObjectDoesNotExist(f"404 response from {url}")

        remaining_requests = resp.headers.get("X-RateLimit-Remaining")
        if resp.status_code == 403 and remaining_requests == "0":
            raise RateLimitExceeded(f"Rate limit exceeded when querying {url}")

        resp.raise_for_status()

        if resp.headers.get("content-type") == "application/json":
            return resp.json()

        return resp.text

    def get_instance_config(
        self, project_id: int, instance: str, config_file: InstanceConfigFile
    ) -> GitLabFile | None:
        """
        Returns the instance config from the Grove cluster repository.
        """
        try:
            file_info = self.get_object(
                f"/projects/{project_id}/repository/files/{config_file.get_path(instance)}?ref={config.grove.main_branch}",
                ttl=get_nonce(),
            )
        except ObjectDoesNotExist:
            return None

        return GitLabFile(file_info)

    def get_pipeline_job_id(self, jobs: list[dict], status: GitLabStatus) -> int:
        """
        Returns the job ID for the provided pipeline status.
        """

        def get_duration_difference(item: dict) -> int:
            return item["duration"] - item["queued_duration"]

        target_jobs = list(filter(lambda j: j["status"] == status, jobs))
        target_job = min(target_jobs, key=get_duration_difference)
        return target_job["id"]

    def get_pipeline_job_logs(self, job_id: int) -> str:
        """
        Returns the job logs for the provided job ID.
        """
        return self.get_object(
            f"/projects/{config.grove.project_id}/jobs/{job_id}/trace",
            ttl=get_ttl_hash(),
        )

    def __trigger_pipeline(self, data: dict) -> requests.Response:
        """
        Triggers a pipeline with the provided data.
        """

        payload = {
            **data,
            "ref": config.grove.main_branch,
            "token": self.trigger_token,
        }

        resp = requests.post(
            f"{self.api_base_url}/projects/{config.grove.project_id}/trigger/pipeline",
            json=payload,
            **self.api_params,
        )

        resp.raise_for_status()
        return resp

    def trigger_instance_create_or_update_pipeline(
        self,
        params: CreateOrUpdatePipelineParams,
    ) -> requests.Response:
        """
        Triggers the instance create pipeline.
        """

        payload = {
            f"GROVE_{config.grove.watched_pr_key}": {
                "applied_settings": params.pull_request.extra_settings,
                "applied_requirements": params.pull_request.tutor_requirements,
                "comments_url": params.pull_request.comments_url,
                "commit_ref": params.pull_request.commit_sha,
                "url": params.pull_request.html_url,
            },
            "TUTOR_EDX_PLATFORM_REPOSITORY": params.edx_platform_url,
            "TUTOR_EDX_PLATFORM_VERSION": params.edx_platform_branch,
            "TUTOR_ELASTICSEARCH_INDEX_PREFIX": params.instance.name,
            "TUTOR_OPENEDX_COMMON_VERSION": params.pull_request.named_release.latest_common_version,
            "variables": {
                "INSTANCE_NAME": params.instance.name,
                "DEPLOYMENT_REQUEST_ID": datetime.now().strftime("%Y%m%d-%H%M"),
                "NEW_INSTANCE_TRIGGER": "0" if params.instance.is_defined else "1",
                "REQUIREMENTS": params.pull_request.tutor_requirements,
            },
        }

        if params.mfe_info:
            payload.update(
                {
                    "TUTOR_GROVE_NEW_MFES": {
                        params.mfe_info.mfe_name: {
                            "repository": params.mfe_info.repository_url,
                            "version": params.mfe_info.branch,
                            "port": params.mfe_info.port,
                        }
                    }
                }
            )

        if settings := yaml.safe_load(params.pull_request.extra_settings):
            for k, v in settings.items():
                if k.startswith("TUTOR_GROVE_"):
                    payload[k] = v

                # Users should be able to specify Grove or Tutor settings separately
                # Anything else goes in config.yml
                if k.startswith("GROVE_") or k.startswith("TUTOR_"):
                    continue

                payload[f"TUTOR_{k}"] = v

        return self.__trigger_pipeline(payload)

    def trigger_instance_destroy_pipeline(
        self, params: DestroyPipelineParams
    ) -> requests.Response | None:
        """
        Triggers the instance destroy pipeline.
        """

        if not params.instance.is_defined:
            logger.warning(
                "Instance %s is not defined, cannot trigger destroy pipeline.",
                params.instance.name,
            )

        payload = {
            "variables": {
                "INSTANCE_NAME": params.instance.name,
                "DEPLOYMENT_REQUEST_ID": datetime.now().strftime("%Y%m%d-%H%M"),
                "DELETE_INSTANCE_TRIGGER": "1",
            },
        }

        return self.__trigger_pipeline(payload)


class GitLabWebhook:
    """
    GitLab webhook parsed from payload and enriched with additional data.

    The additional data is fetched from the GitLab API.
    """

    def __init__(self, client: GitLabClient, payload: dict):
        self.__client = client

        self.object_attributes: dict = payload.get("object_attributes", {})
        self.jobs: list[dict] = payload.get("builds", [])
        self.project: dict = payload.get("project", {})
        self.variables: list[dict] = self.object_attributes.get("variables", [])
        self.source: str = self.object_attributes.get("source")
        self.status: str = self.object_attributes.get("status")
        self.ref: str = self.object_attributes.get("ref")

        # NOTE: The trailing whitespace in the pattern is intentional.
        self.trigger_message_pattern: str = (
            r"\[AutoDeploy\]\[(Update|Create|Delete)\]\ "
        )

    @property
    def instance(self) -> GroveInstance | None:
        """
        Returns the Grove instance.
        """

        instance = GroveInstance(self.__client, self.instance_name)

        if not instance.tutor_config:
            logger.error("Instance %s has no Tutor config.", instance.name)
            return None

        if not instance.grove_config:
            logger.error("Instance %s has no Grove config.", instance.name)
            return None

        if not instance.requirements:
            logger.error("Instance %s has no requirements.", instance.name)
            return None

        return instance

    @property
    def comments_url(self) -> str | None:
        """
        Returns the comments URL from the webhook payload.
        """
        instance = self.instance  # store instance to avoid multiple calls
        data = self.trigger_payload or (
            instance and instance.grove_config.content_as_dict()
        )
        return self.__get_saved_pr_watcher_settings(data or {}, "comments_url")

    @property
    def instance_name(self) -> str | None:
        """
        Returns the instance name from the webhook payload.
        """
        if instance_name := self.__get_variable("INSTANCE_NAME"):
            return instance_name

        if commit_message := self.__get_variable("COMMIT_MESSAGE"):
            pattern = f"{self.trigger_message_pattern}{SANDBOX_INSTANCE_PATTERN}"
            if pattern_match := re.search(pattern, commit_message):
                return pattern_match.group("instance")

        return None

    @property
    def is_pr_instance(self) -> bool:
        """
        Returns True if the instance is a PR instance.
        """
        return self.instance_name.startswith("pr-")

    @property
    def is_deployment_failed(self) -> bool:
        """
        Returns True if the deployment failed.
        """
        return self.__is_deployment_matching(GitLabStatus.FAILED)

    @property
    def is_deployment_succeeded(self) -> bool:
        """
        Returns True if the deployment succeeded.
        """
        return self.__is_deployment_matching(GitLabStatus.SUCCESS)

    @property
    def trigger_payload(self):
        """
        Returns the trigger payload from the webhook payload.
        """
        if payload := self.__get_variable("TRIGGER_PAYLOAD"):
            return json.loads(payload)
        return None

    @property
    def is_valid(self) -> bool:
        """
        Returns True if the webhook is valid.
        """
        return self.is_deployment_failed or self.is_deployment_succeeded

    @property
    def is_instance_create_or_update_pipeline(self) -> bool:
        """
        Returns True if the webhook is for an instance create or update pipeline.
        """
        return re.search(
            r"\[AutoDeploy\]\[(Update|Create)\]\ ",
            self.__get_variable("COMMIT_MESSAGE"),
        )

    @property
    def is_instance_delete_pipeline(self) -> bool:
        """
        Returns True if the webhook is for an instance delete pipeline.
        """
        return re.search(
            r"\[AutoDeploy\]\[Delete\]\ ",
            self.__get_variable("COMMIT_MESSAGE"),
        )

    def __get_saved_pr_watcher_settings(self, data: dict, setting: str) -> Any:
        """
        Returns the saved PR watcher settings using a fallback key.
        """
        return data.get(
            config.grove.watched_pr_key,
            data.get(f"GROVE_{config.grove.watched_pr_key}", {}),
        ).get(setting)

    def __get_variable(self, key: str) -> str | None:
        """
        Returns the value of the variable with the provided key.
        """
        return next(
            (v["value"].strip() for v in self.variables if v["key"] == key),
            None,
        )

    def __is_deployment_matching(self, status: GitLabStatus) -> bool:
        """
        Returns True if the deployment is matching the status and is valid.
        """
        return (
            self.variables
            and self.source == "parent_pipeline"
            and self.status == status.value
            and self.is_pr_instance
            and self.comments_url
            and self.ref == config.grove.main_branch
        )
