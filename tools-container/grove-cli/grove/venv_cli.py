import typer

from grove.instance import Instance
from grove.utils import execute

app = typer.Typer()


@app.command("update")
def venv_update(instance_name: str):
    """
    Updates the virtual environment and installs the packages in
    requirements.txt. If the virtual environment doesn't exist,
    it is created.
    """

    typer.echo(f"Updating virtual environment for {instance_name}")
    instance = Instance(instance_name)
    if not instance.exists():
        typer.echo(f"Instance {instance_name} does not exist. Exiting...")
        return
    instance.venv_update()
    typer.echo("Updating virtual envirnonment complete")


@app.command("reset")
def venv_reset(instance_name: str):
    """
    Delete and recreates the instance's virtual environment.
    """
    instance = Instance(instance_name)
    if not instance.exists():
        typer.echo(f"Instance {instance_name} does not exist. Exiting...")
        return
    typer.echo(f"Resetting virtual environment for {instance_name}")

    instance.venv_reset()
    typer.echo("Resetting virtual environment complete")


@app.command("shell")
def venv_shell(instance_name: str):
    """
    Starts a new Python shell using the instance's virtual environment.
    """
    instance = Instance(instance_name)
    if not instance.exists():
        typer.echo(f"Instance {instance_name} does not exist. Exiting...")
        return
    typer.echo(f"Starting virtual environment shell for  {instance_name}")

    return execute(str(instance.venv_dir / "bin" / "python"))
