import json
from unittest.case import TestCase

from grove.handlers import GitHubWebhookData, GitLabWebhookData

from .utils import FIXTURES_DIR


class TestGitHubWebhookData(TestCase):
    def setUp(self):
        with open(FIXTURES_DIR / "github.json", "r") as file:
            self.payload = json.load(file)

    def test_empty_payload(self):
        webhook_data = GitHubWebhookData({})
        self.assertIsNone(webhook_data.edx_repository)
        self.assertIsNone(webhook_data.branch_name)
        self.assertEqual(webhook_data.merged, False)

    def test_merged_pr(self):
        webhook_data = GitHubWebhookData(self.payload)
        self.assertEqual(webhook_data.edx_repository, "https://github.com/Codertocat/Hello-World.git")
        self.assertEqual(webhook_data.branch_name, "master")
        self.assertEqual(webhook_data.merged, True)

    def test_unmerged_pr(self):
        self.payload["pull_request"]["merged"] = False
        webhook_data = GitHubWebhookData(self.payload)
        self.assertEqual(webhook_data.edx_repository, "https://github.com/Codertocat/Hello-World.git")
        self.assertEqual(webhook_data.branch_name, "master")
        self.assertEqual(webhook_data.merged, False)


class TestGitLabWebhookData(TestCase):
    def setUp(self):
        with open(FIXTURES_DIR / "gitlab.json", "r") as file:
            self.payload = json.load(file)

    def test_empty_payload(self):
        webhook_data = GitLabWebhookData({})
        self.assertIsNone(webhook_data.edx_repository)
        self.assertIsNone(webhook_data.branch_name)
        self.assertEqual(webhook_data.merged, False)

    def test_merged_mr(self):
        webhook_data = GitLabWebhookData(self.payload)
        self.assertEqual(webhook_data.edx_repository, "http://example.com/gitlabhq/gitlab-test.git")
        self.assertEqual(webhook_data.branch_name, "master")
        self.assertEqual(webhook_data.merged, True)

    def test_unmerged_mr(self):
        self.payload["object_attributes"]["action"] = "open"
        webhook_data = GitLabWebhookData(self.payload)
        self.assertEqual(webhook_data.edx_repository, "http://example.com/gitlabhq/gitlab-test.git")
        self.assertEqual(webhook_data.branch_name, "master")
        self.assertEqual(webhook_data.merged, False)
