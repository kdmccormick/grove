####################################################################################################
## Cloudfront resources
####################################################################################################

locals {
  cloudfront_instances = tomap({
    for k, v in toset(var.tutor_instances) : k => v if try(module.edx_instances.configs[k]["GROVE_ENABLE_CLOUDFRONT"], false) != false
  })

  cloudfront_lms_instances = {
    for k, v in local.cloudfront_instances : k => v if try(module.edx_instances.configs[k]["LMS_HOST"], false) != false
  }

  cloudfront_cms_instances = {
    for k, v in local.cloudfront_instances : k => v if try(module.edx_instances.configs[k]["CMS_HOST"], false) != false
  }

  cloudfront_mfe_instances = {
    for k, v in local.cloudfront_instances : k => v if try(module.edx_instances.configs[k]["GROVE_MFE_CDN_ORIGIN"], false) != false
  }
}

module "cloudfront_lms" {
  for_each      = local.cloudfront_lms_instances
  instance_name = each.key
  origin_domain = module.edx_instances.configs[each.key]["LMS_HOST"]
  source        = "../provider-modules/cdn-cloudfront"
  service_name  = "lms"
  providers = {
    aws.virginia = aws.virginia
  }
}

module "cloudfront_cms" {
  for_each      = local.cloudfront_cms_instances
  instance_name = each.key
  origin_domain = module.edx_instances.configs[each.key]["CMS_HOST"]
  source        = "../provider-modules/cdn-cloudfront"
  service_name  = "cms"
  providers = {
    aws.virginia = aws.virginia
  }
}

module "cloudfront_mfe" {
  for_each      = local.cloudfront_mfe_instances
  instance_name = each.key
  origin_domain = module.edx_instances.configs[each.key]["GROVE_MFE_CDN_ORIGIN"]
  alias         = module.edx_instances.configs[each.key]["MFE_HOST"]
  source        = "../provider-modules/cdn-cloudfront"
  service_name  = "mfe"
  providers = {
    aws.virginia = aws.virginia
  }
}

output "cloudfront_lms_domains" {
  value = {
    for instance, _ in local.cloudfront_cms_instances :
    instance => module.cloudfront_lms[instance].cloudfront_domain
  }
}

output "cloudfront_cms_domains" {
  value = {
    for instance, _ in local.cloudfront_cms_instances :
    instance => module.cloudfront_cms[instance].cloudfront_domain
  }
}

output "cloudfront_mfe_domains" {
  value = {
    for instance, _ in local.cloudfront_mfe_instances :
    instance => module.cloudfront_mfe[instance].cloudfront_domain
  }
}

output "cloudfront_mfe_domain_validation_options" {
  value = {
    for instance, _ in local.cloudfront_mfe_instances :
    instance => module.cloudfront_mfe[instance].domain_validation_options
  }
}
