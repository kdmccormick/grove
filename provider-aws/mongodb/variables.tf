variable "mongodbatlas_project_id" { type = string }
variable "mongodbatlas_cluster_name" { type = string }

variable "cluster_name" { type = string }
variable "region_name" { type = string }

variable "provider_name" { type = string }
variable "mongodb_version" { type = string }
variable "instance_size" { type = string }
variable "cluster_type" { type = string }
variable "num_shards" { type = number }
variable "electable_nodes" { type = number }
variable "read_only_nodes" { type = number }
variable "analytics_nodes" { type = number }

variable "backup_enabled" { type = bool }
variable "backup_retention_period" { type = number }

variable "encryption_at_rest" { type = bool }

variable "disk_size_gb" { type = number }
variable "disk_iops" { type = number }
variable "volume_type" { type = string }

variable "auto_scaling_disk_gb_enabled" { type = bool }
variable "auto_scaling_compute_enabled" { type = bool }
variable "auto_scaling_min_instances" { type = number }
variable "auto_scaling_max_instances" { type = number }

variable "vpc_id" { type = string }
variable "vpc_cidr_block" { type = string }
variable "atlas_cidr_block" { type = string }
variable "aws_account_id" { type = string }
variable "route_table_id" { type = string }

variable "users" {
  type = map(object({
    username       = string
    database       = string
    forum_database = string
  }))
  default     = {}
  description = "Map of overrides for the user and database names."
}

locals {
  region_name = upper(replace(var.region_name, "-", "_"))
}
