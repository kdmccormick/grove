variable "bucket_prefix" { type = string }
variable "tags" { type = map(any) }
variable "is_root_objects_public" {
  type    = bool
  default = false
}
variable "allowed_cors_origins" {
  type    = list(string)
  default = ["*"]
}
variable "versioning" {
  type    = bool
  default = true
}
