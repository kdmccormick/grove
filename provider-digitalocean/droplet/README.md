# droplet

This module creates a new Droplet on DigitalOcean.

# Variables

- `cluster_name`: Kubernetes cluster name the Droplet has access to. Default `null`.
- `image`: Droplet image.
- `name`: Name of the Droplet.
- `region`: Droplet region.
- `size`: Droplet size. Default `s-1vcpu-1gb`.
- `backups`: Enable backups for the Droplet. Default `true`.
- `ssh_keys`: List of SSH keys used for the Droplet.
- `tags`: Tags assigned to the Droplet.
- `volume_ids`: List of volume IDs attached to the Droplet.
- `user_data`: User data run on the Droplet at the first start.

# Output

This module doesn't provide any output.

- `droplet_urn`: Unique resource number of the Droplet.
- `droplet_id`: ID of the Droplet.
- `droplet_ipv4`: IPV4 address of the Droplet.
- `droplet_key_pair`: SSH key pair created for the Droplet. 
