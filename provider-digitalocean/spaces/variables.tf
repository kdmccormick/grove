variable "bucket_prefix" { type = string }
variable "do_region" { type = string }
variable "is_root_objects_public" {
  type    = bool
  default = false
}
variable "allowed_cors_origins" {
  type    = list(string)
  default = ["*"]
}
variable "force_destroy" {
  type    = bool
  default = true
}
variable "versioning" {
  type    = bool
  default = true
}
