terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

resource "random_id" "bucket_suffix" {
  byte_length = 8
}

resource "digitalocean_spaces_bucket" "spaces_bucket" {
  name          = "${var.bucket_prefix}-${random_id.bucket_suffix.dec}"
  region        = var.do_region
  acl           = "private"
  force_destroy = var.force_destroy

  # Versioning protects us from accidental file deletion and override. Lifecycle rules clean up old file versions.
  versioning {
    enabled = var.versioning
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = var.allowed_cors_origins
  }

  lifecycle_rule {
    enabled = true

    expiration {
      # Delete files that have no available versions.
      expired_object_delete_marker = true
    }

    noncurrent_version_expiration {
      days = 30
    }
  }
}

# Add policy so that anonymous users can add/view/edit
# uploads to the discussion forum
resource "digitalocean_spaces_bucket_policy" "public_root_object_policy" {
  bucket = digitalocean_spaces_bucket.spaces_bucket.name
  region = var.do_region
  count  = var.is_root_objects_public ? 1 : 0

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "ForumUploads",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : [
          "s3:GetObject"
        ],
        "Resource" : [
          "arn:aws:s3:::${digitalocean_spaces_bucket.spaces_bucket.name}/*",
        ]
      }
    ]
  })
}
