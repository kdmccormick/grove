terraform {
  required_providers {
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = "~> 1.0"
    }
  }
}

locals {
  service_namespace = "openfaas"
  fn_namespace      = "openfaas-fn"
}

resource "kubernetes_namespace" "k8s_service_namespace" {
  metadata {
    name = local.service_namespace
  }
}

resource "kubernetes_namespace" "k8s_functions_namespace" {
  metadata {
    name = local.fn_namespace
  }
}

resource "helm_release" "k8s_openfaas" {
  name       = "openfaas"
  repository = "https://openfaas.github.io/faas-netes"
  chart      = "openfaas"
  version    = var.openfaas_chart_version
  namespace  = local.service_namespace
  depends_on = [kubernetes_namespace.k8s_service_namespace, kubernetes_namespace.k8s_functions_namespace]

  set {
    name  = "functionNamespace"
    value = local.fn_namespace
  }

  set {
    name  = "basic_auth"
    value = "false"
  }

  set {
    name  = "exposeServices"
    value = "false"
  }

  timeout = 600
}

resource "random_password" "ingress_auth" {
  length           = 32
  special          = false
  override_special = ",.-_!"
}

resource "htpasswd_password" "ingress_auth" {
  password = random_password.ingress_auth.result
  salt     = substr(sha512(random_password.ingress_auth.result), 0, 8)
}

resource "kubernetes_secret" "ingress_auth" {
  metadata {
    name      = "${local.service_namespace}-basic-auth"
    namespace = local.service_namespace
    labels = {
      app = "k8s-ingress"
    }
  }

  data = {
    "admin" = htpasswd_password.ingress_auth.bcrypt
  }
}

resource "helm_release" "k8s_openfaas_cron_connector" {
  name       = "openfaas-cron-connector"
  repository = "https://openfaas.github.io/faas-netes"
  chart      = "cron-connector"
  version    = var.openfaas_connector_chart_version
  namespace  = local.service_namespace

  set {
    name  = "gatewayURL"
    value = "http://gateway.${local.service_namespace}.svc.cluster.local:8080"
  }

  depends_on = [
    kubernetes_secret.cron_basic_auth
  ]
}

resource "kubernetes_secret" "cron_basic_auth" {
  depends_on = [kubernetes_namespace.k8s_service_namespace]
  metadata {
    name      = "basic-auth"
    namespace = local.service_namespace
  }

  # Username and password can be anything, since we are doing basic auth
  # through the internal gatewayURL, so basic auth is not protected.
  data = {
    "basic-auth-user"     = "username",
    "basic-auth-password" = "password",
  }
}

resource "kubernetes_ingress_v1" "openfaas_ingress" {
  depends_on = [
    helm_release.k8s_openfaas
  ]

  metadata {
    name      = "${local.service_namespace}-ingress"
    namespace = local.service_namespace
    annotations = {
      "nginx.ingress.kubernetes.io/auth-type"        = "basic",
      "nginx.ingress.kubernetes.io/auth-secret-type" = "auth-map",
      "nginx.ingress.kubernetes.io/auth-secret"      = "${local.service_namespace}/${kubernetes_secret.ingress_auth.metadata.0.name}"
      "nginx.ingress.kubernetes.io/auth-realm"       = "Authentication is required"
      "nginx.ingress.kubernetes.io/ssl-redirect"     = "true"
      "nginx.ingress.kubernetes.io/enable-cors"      = "true"
      "kubernetes.io/tls-acme"                       = "true"
      "cert-manager.io/cluster-issuer"               = "letsencrypt-prod"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      secret_name = "${local.service_namespace}-ingress"
      hosts = [
        "openfaas.${var.cluster_domain}"
      ]
    }

    rule {
      host = "openfaas.${var.cluster_domain}"
      http {
        path {
          backend {
            service {
              name = "gateway"
              port {
                number = 8080
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}
