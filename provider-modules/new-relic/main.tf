terraform {
  required_providers {
    newrelic = {
      source = "newrelic/newrelic"
    }
  }
}

module "new_relic_instances" {
  for_each = var.tutor_configs
  source   = "./new-relic-instance"

  cluster_name          = var.cluster_name
  instance_name         = each.key
  new_relic_account_id  = var.new_relic_account_id
  new_relic_api_key     = var.new_relic_api_key
  new_relic_region_code = var.new_relic_region_code
  email_recipients      = var.email_recipients
  tutor_config          = var.tutor_configs[each.key]
}


resource "newrelic_notification_destination" "email_destinations" {
  account_id = var.new_relic_account_id
  name       = "${var.cluster_name}-emaildest"
  type       = "EMAIL"

  property {
    key   = "email"
    value = var.email_recipients
  }
}

resource "newrelic_notification_channel" "new_relic_notification" {
  account_id     = var.new_relic_account_id
  name           = "${var.cluster_name}-notifications"
  type           = "EMAIL"
  destination_id = newrelic_notification_destination.email_destinations.id
  product        = "IINT" // (Workflows)

  property {
    key   = "subject"
    value = "[{{state}}] {{ issueTitle }} - Issue {{issueId}}"
  }
}

resource "newrelic_workflow" "workflow" {
  name                  = "${var.cluster_name}-workflow"
  muting_rules_handling = "NOTIFY_ALL_ISSUES"

  issues_filter {
    name = "${var.cluster_name}-filter"
    type = "FILTER"

    predicate {
      attribute = "labels.policyIds"
      operator  = "EXACTLY_MATCHES"
      values    = [for k, v in module.new_relic_instances : v.alert_policy_id]
    }
  }

  destination {
    channel_id = newrelic_notification_channel.new_relic_notification.id
  }
}
