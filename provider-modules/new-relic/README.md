# new-relic

This module configures [New Relic](https://new-relic.com/) synthetic alerts for each instance
managed by Grove.

Alerts are created for the following URLs for each instance:

- `{{LMS_HOST}}/heartbeat`
- `{{LMS_HOST}}/heartbeat?extended`
- `{{CMS_HOST}}/heartbeat`
- `{{PREVIEW_LMS_HOST}}/heartbeat`

# Variables

- `new_relic_account_id` - The id of the account in use. Set to 0, to disable New Relic.
- `new_relic_api_key` - The generated New Relic API key with User permissions. See https://registry.terraform.io/providers/newrelic/newrelic/latest/docs#resources for the permissions required when adding new resources.
- `new_relic_region_code`. The region your account exists in. Either `US` or `EU`
- `cluster_name`: Your cluster name. Used for naming resources and disambiguation.
- `tutor_configs`: A list of Tutor configurations for each instance. Required information will be pulled from here.
- `email_recipients`: A comma-separated list of emails that will receive the alert notifications.

# Output

None

