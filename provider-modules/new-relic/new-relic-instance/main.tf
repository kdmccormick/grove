terraform {
  required_providers {
    newrelic = {
      source = "newrelic/newrelic"
    }
  }
}

locals {
  urls = compact(flatten([
    "https://${var.tutor_config["LMS_HOST"]}/heartbeat",
    "https://${var.tutor_config["CMS_HOST"]}/heartbeat",
    "https://${var.tutor_config["LMS_HOST"]}/heartbeat?extended",
    try("https://${var.tutor_config["PREVIEW_LMS_HOST"]}/heartbeat", null),
  ]))
}

resource "newrelic_alert_policy" "instance_alert_policy" {
  name = "${var.cluster_name}-${var.instance_name}"
}

resource "newrelic_synthetics_monitor" "monitored_urls" {
  for_each         = toset(local.urls)
  status           = "ENABLED"
  name             = each.value
  period           = "EVERY_5_MINUTES"
  uri              = each.value
  type             = "SIMPLE"
  locations_public = ["US_EAST_1"]

  treat_redirect_as_failure = true
  bypass_head_request       = true
  verify_ssl                = true
}

resource "newrelic_synthetics_alert_condition" "foo" {
  for_each  = toset(local.urls)
  policy_id = newrelic_alert_policy.instance_alert_policy.id

  name       = each.value
  monitor_id = newrelic_synthetics_monitor.monitored_urls[each.key].id
}
