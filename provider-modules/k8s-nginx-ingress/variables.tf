variable "cluster_provider" {
  type = string
  validation {
    condition = contains([
      "aws",
      "digitalocean",
      "minikube"
    ], var.cluster_provider)
    error_message = "The cluster_provider value Must be one of: 'aws', 'digitalocean', 'minikube'."
  }
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain name of the cluster."
}

variable "ingress_namespace" {
  type = string
}

variable "global_404_html_path" {
  type        = string
  default     = ""
  description = "Path in tools-container to the html page to show when provisioning instances or if there's a 404 on the ingress."
}

variable "admission_webhooks_enabled" {
  type    = bool
  default = true
}

variable "nginx_resource_quotas" {
  type = object({
    limits   = optional(map(string))
    requests = optional(map(string))
  })

  default = {}

  description = "Kubernetes resource configuration for the NGINX ingress pods."
}
