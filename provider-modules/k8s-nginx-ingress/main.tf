locals {
  global_404_html_path    = var.global_404_html_path != "" ? var.global_404_html_path : "${path.module}/404.html"
  nginx_resource_limits   = try(var.nginx_resource_quotas["limits"], null)
  nginx_resource_requests = try(var.nginx_resource_quotas["requests"], null)
  nginx_version           = "4.7.1"  # https://github.com/kubernetes/ingress-nginx/releases
  cert_manager_version    = "1.12.3" # https://artifacthub.io/packages/helm/cert-manager/cert-manager
}

resource "kubernetes_config_map" "custom_error_pages" {
  metadata {
    name      = "custom-error-pages"
    namespace = var.ingress_namespace
  }

  data = {
    "404" = file(local.global_404_html_path)
  }
}

resource "helm_release" "nginx_ingress" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = local.nginx_version
  namespace  = var.ingress_namespace
  timeout    = 600

  set {
    name  = "rbac.create"
    value = true
  }

  set {
    name  = "controller.admissionWebhooks.enabled"
    value = var.admission_webhooks_enabled
  }

  values = [
    templatefile("${path.module}/ingress-values.yml", {
      cluster_domain    = var.cluster_domain,
      cluster_provider  = var.cluster_provider,
      resource_limits   = local.nginx_resource_limits,
      resource_requests = local.nginx_resource_requests
    })
  ]
}

# install cert manager
resource "helm_release" "ingress_cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = local.cert_manager_version
  namespace  = var.ingress_namespace
  values = [
    file("${path.module}/cert_manager.yml")
  ]
  depends_on = [helm_release.nginx_ingress]
}
