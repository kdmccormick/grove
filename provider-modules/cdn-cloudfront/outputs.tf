output "cloudfront_domain" {
  value = aws_cloudfront_distribution.cloudfront_distribution.domain_name
}

output "domain_validation_options" {
  value = try(aws_acm_certificate.alias_certificate[0].domain_validation_options, null)
}
