# cdn-cloudfront

This configures an AWS CDN using CloudFront to cache assets from a given origin URL.

When enabled for an instance, CloudFront CDN's will be created for the static assets in the LMS and CMS.

This module has been modified from https://github.com/open-craft/terraform-scripts/tree/main/optional/cloudfront_cdn.

# Variables

- `instance_name`: The name of the instance to create the CDN for. Should be all lowercase, slugified.
- `service_name`: The service to create the CDN for, eg. `lms` or `cms`. It's used for disambiguation.
- `origin_domain`: The domain the CDN will fetch the static files from.
- `cache_expiration`: TTL of the cache in seconds. Defaults to `31536000`.

# Outputs

- `cloudfront_domain`: The generated CloudFront domain that you should use as a replacement for the static files.
