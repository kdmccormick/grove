####################################################################################################
## Elasticsearch Certs: Resources for creating Elasticsearch certificates
####################################################################################################

resource "tls_private_key" "elasticsearch_root_ca_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_self_signed_cert" "elasticsearch_root_ca_cert" {
  private_key_pem   = tls_private_key.elasticsearch_root_ca_key.private_key_pem
  is_ca_certificate = true

  subject {
    common_name         = "groveca"
    organization        = "OpenCraft"
    organizational_unit = "Grove"
    locality            = "Berlin"
    province            = "Berlin"
    country             = "DE"
  }

  validity_period_hours = 24 * 365 * 5

  allowed_uses = [
    "digital_signature",
    "cert_signing",
    "crl_signing",
  ]
}

resource "tls_private_key" "elasticsearch_admin_key" {
  algorithm = "RSA"
  rsa_bits  = "2048"
}

resource "tls_cert_request" "elasticsearch_admin_csr" {
  private_key_pem = tls_private_key.elasticsearch_admin_key.private_key_pem

  dns_names = ["elasticsearch-master.elasticsearch.svc.cluster.local"]
  subject {
    common_name         = "elasticsearch-master.elasticsearch.svc.cluster.local"
    organization        = "OpenCraft"
    organizational_unit = "Grove"
    locality            = "Berlin"
    province            = "Berlin"
    country             = "DE"
  }
}

resource "tls_locally_signed_cert" "elasticsearch_admin_cert" {
  cert_request_pem   = tls_cert_request.elasticsearch_admin_csr.cert_request_pem
  ca_private_key_pem = tls_private_key.elasticsearch_root_ca_key.private_key_pem
  ca_cert_pem        = tls_self_signed_cert.elasticsearch_root_ca_cert.cert_pem

  validity_period_hours = 5 * 365 * 24

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}

resource "kubernetes_secret" "elasticsearch_certificates" {
  depends_on = [kubernetes_namespace.elasticsearch_namespace]
  metadata {
    name      = "elasticsearch-certificates"
    namespace = local.elasticsearch_namespace
  }
  data = {
    "tls.key"          = tls_private_key.elasticsearch_admin_key.private_key_pem_pkcs8
    "tls.crt"          = tls_locally_signed_cert.elasticsearch_admin_cert.cert_pem
    "ca.crt"           = tls_self_signed_cert.elasticsearch_root_ca_cert.cert_pem
    "server-chain.crt" = "${tls_locally_signed_cert.elasticsearch_admin_cert.cert_pem}{tls_self_signed_cert.elasticsearch_root_ca_cert.cert_pem}"
  }
  type = "Opaque"
}
