# k8s-elasticsearch

This module installs an [Elasticsearch](https://elasticsearch.co) cluster.

To enable the module, set `enable_shared_elasticsearch=true`. When applied an ES cluster
will be deployed under the `elasticsearch` namespace.

Multi-tenancy works by creating a user for each Open edX instance and assigning a prefix to the indices
for that user. For example, an instance named `demo_instance` will be only allowed access to indices
of the form `demo_instance-abcd-*`. The `abcd` is random and exists to disambiguate the indices so that `demo-*` doesn't
have access to all `demo_instance`'s indices.

In order to make use of this module the following changes need to be incorporated as the PRs are still open:

- https://github.com/open-craft/tutor-forum/tree/keith/shared-elasticsearch
- https://github.com/open-craft/edx-search/tree/keith/shared-elasticsearch
- https://github.com/open-craft/cs_comments_service/tree/keith/prefix-elasticsearch-indexes

Once enabled, check that you have the following settings defined in your `config.yml`:

```yaml
GROVE_ENABLE_SHARED_ELASTICSEARCH: true
ELASTICSEARCH_HOST: set-via-environment
ELASTICSEARCH_HTTP_AUTH: set-via-environment
ELASTICSEARCH_INDEX_PREFIX: set-via-environment
ELASTICSEARCH_CA_CERT_PEM: set-via-environment
```

# Variables

The module has the following inputs:

- `elasticsearch_config` a map with the keys:
  - `heap_size`: Defaulting to `2g`, this is the minimum and maximum heap size that will be used for each node in the cluster.
  - `replicas`: Defaults to `2`. The number of replicas to create for the cluster. Do not set this number higher than your number of nodes as each pod is meant to run on a dedicated node.
  - `search_queue_size`: Defaults to `5000`. This number determines the throughput (and resource usage) of your cluster. It should be increased only if there's more CPU available.
  - `cpu_limit`: Defaults to `4000m`.
  - `memory_limit`: Defaults to `4Gi`.

# Output

- `admin_password`: The generated Elasticsearch admin password
- `instances`: The password and prefix for each of the Open edX instances.
