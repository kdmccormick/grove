# The ElasticSearch admin password - access it with "./tf output -raw elasticsearch_admin_password"
output "admin_password" {
  value     = random_password.admin_password.result
  sensitive = true
}

output "ca_cert_pem" {
  value = tls_self_signed_cert.elasticsearch_root_ca_cert.cert_pem
}

output "instances" {
  sensitive = true
  value = {
    for instance in var.tutor_instances :
    instance => {
      username     = instance,
      password     = random_password.elasticsearch_passwords[instance].result,
      index_prefix = "${instance}-${random_string.elasticsearch_prefix_separator[instance].result}"
    }
  }
}
