# k8s-cert-manager

This module installs a Cert manager in a namespaces to generate SSL Certificates used by ingress controllers.

# Variables

- `app_namespace`: Namespace of the application.
- `domain`: Domain name of the application.
- `lets_encrypt_notification_inbox`: Email to send any email notifications about Letsencrypt

# Output

This module doesn't provide any output.
