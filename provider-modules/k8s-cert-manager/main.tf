terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
  }
}

# If using the kubectl_manifest resource then we get an error
# every time we do a `terraform plan`:
# No matches for kind "ClusterIssuer" in group "cert-manager.io"
# See https://github.com/hashicorp/terraform-provider-kubernetes/issues/1367
resource "kubectl_manifest" "cert_manager" {
  yaml_body = templatefile("${path.module}/cert-manager-letsencrypt.yml", {
    namespace          = var.namespace,
    notification_email = var.lets_encrypt_notification_inbox
  })
}
